// Sieve of Eratosthenes, will take threshold number
// and returns a list all primes below that number
function primegen(thresh) {
  const isPrime = [];
  const primes = [];
  // generate a list of booleans all set to true initially
  // the array is indexed from 2 to limit representing all numbers
  // e.g. [true, true, true, true] = [2, 3, 4, 5]
  for (var i = 0; i <= thresh; i++) {
    isPrime.push(true);
  }
  // remove duplicate primes
  for (var i = 2; i <= thresh; i++) {
    for (let j = i * i; j <= thresh; j += i) {
      isPrime[j] = false;
    }
  }
  // make our array of primes
  for (var i = 2; i <= thresh; i++) {
    if (isPrime[i]) {
      primes.push(i);
    }
  }
  return primes;
}
console.log(primegen(10));

function cumulativeSum(array) {
  const result = [array[0]];
  for (let i = 1; i < array.length; i++) {
    result[i] = result[i - 1] + array[i];
  }
  return result;
}
console.log(cumulativeSum([1, 2, 3, 4]));

function maxPrimeSum(thresh) {
  let result = 0;
  let primeCount = 0;
  const primeSum = [];
  // call primegen to generate a list of primes based on the thresh
  const primes = primegen(thresh);

  primeSum[0] = 0;
  for (var i = 0; i < primes.length; i++) {
    primeSum[i + 1] = primeSum[i] + primes[i];
  }
  for (var i = 0; i < primeSum.length; i++) {
    for (let j = i - (primeCount + 1); j >= 0; j--) {
      if (primeSum[i] - primeSum[j] > thresh) break;
      if (primes.indexOf(primeSum[i] - primeSum[j]) >= 0) {
        primeCount = i - j;
        result = primeSum[i] - primeSum[j];
      }
    }
  }
  return [result, primeCount];
}
console.log(maxPrimeSum(1000));
