// Sieve of Eratosthenes, will take threshold number
// and returns a list all primes below that number
function primegen(thresh) {
  const isPrime = [];
  const primes = [];
  // generate a list of booleans all set to true initially
  // the array is indexed from 2 to limit representing all numbers
  // e.g. [true, true, true, true] = [2, 3, 4, 5]
  for (var i = 0; i <= thresh; i++) {
    isPrime.push(true);
  }
  // remove duplicate primes
  for (var i = 2; i <= thresh; i++) {
    for (let j = i * i; j <= thresh; j += i) {
      isPrime[j] = false;
    }
  }
  // make our array of primes
  for (var i = 2; i <= thresh; i++) {
    if (isPrime[i]) {
      primes.push(i);
    }
  }
  return primes;
}
console.log(primegen(10));
