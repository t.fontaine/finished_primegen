function cumulativeSum(array) {
  const result = [array[0]];
  for (let i = 1; i < array.length; i++) {
    result[i] = result[i - 1] + array[i];
  }
  return result;
}
console.log(cumulativeSum([1, 2, 3, 4]));
